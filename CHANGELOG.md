# Changelog

## Released 1.0.1 (2022-10-04)

- Move glue.ligolw to python-ligo-lw.

- Rename mbtaonline to mbta consistently in the package data.

## Released 1.0.0 (2022-02-01)

- Remove mass gap from the source categories. Templates > 3 solar mass
  is considered a black hole. There are only 3 astrophysical categories:
  BNS, NSBH, BBH. The previous mass gap counts are equally distributed
  between the NSBH and BBH categories. This does not affect previous
  values significantly. Further fine-grained refinement to the counts
  will have to be done from more recent data.

- Remove a redundancy between package and test data. Keep only one
  copy of counts and snr threshold information as package data. Change
  unittests accordingly.

## Released 1.0.0dev1 (2022-01-21)

- Remove all em-bright components, docs, tests.
- Speed up imports by up to a second by replacing uses of `pkg_resources` with
  the new Python standard library module `importlib.resources` (or, for Python
  < 3.7, the backport `importlib_resources`).
- Drop python < 3.7 support.
- pin scipy<=1.7.3, python-ligolw<=1.7.0 to allow python 3.7 support.
- Drop dependency handling using pipenv

## Released 0.8.2 (2020-04-29)

- Update to `scikit-learn==0.22.2.post1`. Retrain classifiers.
- Store new pickled objects compatible with new version.
- Update Jupyter notebook using new classifiers.
- Update meta information.

## Released 0.8.1 (2019-08-28)
- Added documentation for `em_bright` module and a notebook showing the
  behavior of the `HasNS` and `HasRemnant` predictors.
- Make `computeCompactness` return 0.5 when lalsimulation gives
  a RuntimeError when computing NS radius.
- Added 2H classifiers as package data

## Released 0.8.0 (2019-08-16)
- Adding computation of EM-Bright probabilities using source-frame samples.
- Neutron star compactness can now be computed from equation of
  states available in lalsimulation.
- Refactored methods `computeDiskMass` and `computeCompactness`.
- Added numpy docstrings to `computeDiskMass` methods.
- Added p-astro package data to get rid of emfollow/data directory.
- Added a `__init__.py` to data directory

## Released 0.7 (2019-05-14)
- Undocumented

## Released 0.6 (2019-05-08)
- Undocumented

## Released 0.5 (2019-05-01)
- changed the upper threshold for the neutron star mass to 3.0 solar mass.

- Ported over P_astro code from gwcelery. Included relevant unit tests,
  and data files pertaining to unit tests.

- Added to MANIFEST.in to point to P_astro related data files

## Released 0.4 (2019-03-01)
-  added MANIFEST.in file.

-  added inlclude_package_data in setup.py

-   defined new command line tools for conducting classification
    using random forest classifier.

-   added DAG writer and config files to run p-astro and em-bright
    using gstlal injection sets.

-   added disk-mass computation script for classification of EM-Bright
    events.

-   added classifiers as package data.

## Released 0.3 (2018-07-31)
-   Undocumented

## Released 0.2 (2018-07-23)
-   Added new methods

-   Methods compute mean and pastro

-   pastro computed for new event using mean of Poisson counts

## Released 0.1 (2018-07-01)

-   Added p_astro computation module

-   Preliminary Unit Test

-   Sphinx Documentation

-   Added gitlab CI to deplay docs and peform the unittests
